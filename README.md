JPS Webhook Display Field Sync
=======================================
This project makes it easier to add extra display fields to webhooks. Since they already used the display field info as advanced searches I made this to sync any advanced search display fields with the "Webhook: WEBHOOK_NAME" naming convention to a corresponding WEBHOOK_NAME webhook. This project works for computer, mobile device, and user.

## Reasoning for script
It's currently kind of annoying to adjust the display fields of webhooks so this puts it in the GUI and allows people without API experience to edit them as well.

## Usage
Create an advanced search with the name "Webhook: WEBHOOK_NAME" and a webhook with the name WEBHOOK_NAME. When this script is run it will sync any display fields in that advanced search to the corresponding webhook. If you try to sync an advanced search with any display fields in the "Export Only" category set to a webhook it will fail.
> Advanced search "Webhook: My Cool Webhook" would correspond with webhook "My Cool Webhook"

### Local
Export the variables listed below into your environment:
* All variable values are described below.
* The first command will make your bash history ignore commands with 
whitespace infront of them. This will make it so your JPS_PASSWORD isn't 
in 
your terminal history.
```console
setopt HIST_IGNORE_SPACE
export JPS_URL=https://example.jamfcloud.com
export JPS_USERNAME=exampleUsername
 export JPS_PASSWORD=examplePassword
```

Run these commands to install all dependencies and run the script:
```console
pip install pipenv
pipenv install --deploy
pipenv run python ./jps_webhook_display_fields_sync.py
```

### GitLab
Make a new schedule with the JPS_URL variable set. Run the script after you make your changes in the JPS to sync across the display fields in the advances search to the corresponding webhook.

## Variables

### JPS_URL
* URL of your JPS Server
> https://example.jamfcloud.com

### JPS_USERNAME / JPS_PASSWORD
JPS account credentials with the following permissions set:
* Jamf Pro Server Objects
  * Advanced Computer Searches
    * Read
  * Advanced Mobile Device Searches
    * Read
  * Advanced User Searches
    * Read
  * Webhooks
    * Read
    * Update

## Authors
Bryan Weber (bweber26@cvtc.edu)

## Copyright
JPS Webhook Display Fields Sync is Copyright {current_year} Chippewa Valley Technical College. It is provided under the MIT License. For more information, please see the LICENSE file in this repository.
