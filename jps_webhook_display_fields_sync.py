import os
import xmltodict

from jps_api_wrapper.classic import Classic
from jps_api_wrapper.pro import Pro
from jps_api_wrapper.request_builder import NotFound


def main():
    JPS_URL = os.environ["JPS_URL"]
    JPS_USERNAME = os.environ["JPS_USERNAME"]
    JPS_PASSWORD = os.environ["JPS_PASSWORD"]

    with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic, Pro(
        JPS_URL, JPS_USERNAME, JPS_PASSWORD
    ) as pro:
        advanced_mobile_device_searches = pro.get_advanced_mobile_device_searches()[
            "results"
        ]
        advanced_computer_searches = classic.get_advanced_computer_searches()[
            "advanced_computer_searches"
        ]
        advanced_user_searches = classic.get_advanced_user_searches()[
            "advanced_user_searches"
        ]

        """Run for advanced mobile device searches"""
        for search in advanced_mobile_device_searches:
            if "Webhook:" in search["name"]:
                try:
                    search_xml = xmltodict.parse(
                        classic.get_advanced_mobile_device_search(
                            name=search["name"], data_type="xml"
                        )
                    )
                    webhook = xmltodict.parse(
                        classic.get_webhook(
                            name=search["name"].replace("Webhook: ", ""),
                            data_type="xml",
                        )
                    )
                    """If no display fields are currently set"""
                    if (
                        webhook["webhook"]["enable_display_fields_for_group_object"]
                        == "false"
                    ):
                        webhook["webhook"][
                            "enable_display_fields_for_group_object"
                        ] = "true"
                        webhook["webhook"]["display_fields"] = search_xml[
                            "advanced_mobile_device_search"
                        ]["display_fields"]
                        classic.update_webhook(
                            xmltodict.unparse(webhook), webhook["webhook"]["id"]
                        )
                    """
                    If the advances search display fields don't match the
                    webhooks display fields
                    """
                    if (
                        search_xml["advanced_mobile_device_search"]["display_fields"][
                            "display_field"
                        ]
                        != webhook["webhook"]["display_fields"]["display_field"]
                    ):
                        webhook["webhook"]["display_fields"] = search_xml[
                            "advanced_mobile_device_search"
                        ]["display_fields"]
                        classic.update_webhook(
                            xmltodict.unparse(webhook), webhook["webhook"]["id"]
                        )
                    else:
                        print(
                            f"Nothing to update for webhook {webhook['webhook']['name']}."
                        )
                except NotFound:
                    print(
                        f"Advanced search {search['name']} does not have an",
                        "associated webhook.",
                    )

        """Run for advanced computer searches"""
        for search in advanced_computer_searches:
            if "Webhook:" in search["name"]:
                try:
                    search_xml = xmltodict.parse(
                        classic.get_advanced_computer_search(
                            name=search["name"], data_type="xml"
                        )
                    )
                    webhook = xmltodict.parse(
                        classic.get_webhook(
                            name=search["name"].replace("Webhook: ", ""),
                            data_type="xml",
                        )
                    )
                    """If no display fields are currently set"""
                    if (
                        webhook["webhook"]["enable_display_fields_for_group_object"]
                        == "false"
                    ):
                        webhook["webhook"][
                            "enable_display_fields_for_group_object"
                        ] = "true"
                        webhook["webhook"]["display_fields"] = search_xml[
                            "advanced_computer_search"
                        ]["display_fields"]
                        classic.update_webhook(
                            xmltodict.unparse(webhook), webhook["webhook"]["id"]
                        )
                    """
                    If the advances search display fields don't match the
                    webhooks display fields
                    """
                    if (
                        search_xml["advanced_computer_search"]["display_fields"][
                            "display_field"
                        ]
                        != webhook["webhook"]["display_fields"]["display_field"]
                    ):
                        webhook["webhook"]["display_fields"] = search_xml[
                            "advanced_computer_search"
                        ]["display_fields"]
                        classic.update_webhook(
                            xmltodict.unparse(webhook), webhook["webhook"]["id"]
                        )
                    else:
                        print(
                            f"Nothing to update for webhook {webhook['webhook']['name']}."
                        )
                except NotFound:
                    print(
                        f"Advanced search {search['name']} does not have an",
                        "associated webhook.",
                    )

        """Run for advanced user searches"""
        for search in advanced_user_searches:
            if "Webhook:" in search["name"]:
                try:
                    search_xml = xmltodict.parse(
                        classic.get_advanced_user_search(
                            name=search["name"], data_type="xml"
                        )
                    )
                    webhook = xmltodict.parse(
                        classic.get_webhook(
                            name=search["name"].replace("Webhook: ", ""),
                            data_type="xml",
                        )
                    )
                    """If no display fields are currently set"""
                    if (
                        webhook["webhook"]["enable_display_fields_for_group_object"]
                        == "false"
                    ):
                        webhook["webhook"][
                            "enable_display_fields_for_group_object"
                        ] = "true"
                        webhook["webhook"]["display_fields"] = search_xml[
                            "advanced_user_search"
                        ]["display_fields"]
                        classic.update_webhook(
                            xmltodict.unparse(webhook), webhook["webhook"]["id"]
                        )
                    """
                    If the advances search display fields don't match the
                    webhooks display fields
                    """
                    if (
                        search_xml["advanced_user_search"]["display_fields"][
                            "display_field"
                        ]
                        != webhook["webhook"]["display_fields"]["display_field"]
                    ):
                        webhook["webhook"]["display_fields"] = search_xml[
                            "advanced_user_search"
                        ]["display_fields"]
                        classic.update_webhook(
                            xmltodict.unparse(webhook), webhook["webhook"]["id"]
                        )
                    else:
                        print(
                            f"Nothing to update for webhook {webhook['webhook']['name']}."
                        )
                except NotFound:
                    print(
                        f"Advanced search {search['name']} does not have an",
                        "associated webhook.",
                    )


if __name__ == "__main__":
    main()
